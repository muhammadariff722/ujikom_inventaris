<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Web Inventaris</title>
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Web Booster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->
    
	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	
	<!--web font-->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
	<!--//web font-->

</head>

<body>
<header>	
<div class="container">
<!-- nav -->
		<nav class="navbar navbar-expand-lg navbar-light py-4">
			<!-- logo -->
			<h1>
				<a class="navbar-brand" href="index.html"> <i class="fas fa-globe"></i>
					Web <span>Inventaris</span>
				</a>
			</h1>
			<!-- //logo -->
			<button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<!-- main nav -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-lg-auto text-center">
					<li class="nav-item active mr-lg-2">
						<a class="nav-link" href="index.php">Home
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<li class="last">
						<a class="btn btn-primary" href="login.php">Login</a>
					</li>
				</ul>
			</div>
			<!-- //main nav -->
		</nav>
		<!-- //nav -->
</div>
</header>
		
<!-- banner -->
<div class="banner">
	<div class="banner-layer">
		<div class="container">
			<div class="agile_banner_info">
				<div class="agile_banner_info1">
					<div id="typed-strings" class="agileits_w3layouts_strings">
						<p>Selamat <i>Datang.</i></p>
						<p>Telah <i>Masuk Web</i> Inventaris.</p>
						<p>Sekolah <i>Kami.</i></p>
					</div>
					<span id="typed" style="white-space:pre;"></span>
				</div>
			<div class="banner_agile_para">
				<p>Web Inventaris ini berguna untuk memudahkan sekolah atas menyimpan barang barang yang ada pada sekolah dan disini juga anda bisa meminjam atau mengetahui stok barang apa aja yang ada pada sekolah.</p>
			</div>
			
			</div>
		</div>
	</div>
</div>
<!-- //banner -->	



	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	
	
	<!-- banner-type-text -->
		<script src="js/typed.js" type="text/javascript"></script>
		<script>
			$(function(){

				$("#typed").typed({
					// strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
					stringsElement: $('#typed-strings'),
					typeSpeed: 30,
					backDelay: 500,
					loop: false,
					contentType: 'html', // or text
					// defaults to false for infinite loop
					loopCount: false,
					callback: function(){ foo(); },
					resetCallback: function() { newTyped(); }
				});

				$(".reset").click(function(){
					$("#typed").typed('reset');
				});

			});

			function newTyped(){ /* A new typed object */ }

			function foo(){ console.log("Callback"); }
		</script>
	<!-- //banner-type-text -->
	
	<!-- Stats-Number-Scroller-Animation-JavaScript -->
	<script src="js/waypoints.min.js"></script> 
	<script src="js/counterup.min.js"></script> 
	<script>
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
				delay: 10,
				time: 1000 
			});
		});
	</script>
	<!-- //Stats-Number-Scroller-Animation-JavaScript -->

	<!-- flexslider --><!-- for testimonials -->
	<script defer src="js/jquery.flexslider.js"></script>
	<!--Start-slider-script-->
		<script type="text/javascript">
		
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	  </script>
	<!--End-slider-script-->
	<!-- //flexslider --><!-- //for testimonials -->

	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

</body>
</html>