<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
		table {border-collapse:collapse; table-layout:fixed;width: 550px:}
		table td {word-wrap:break-word;width: 20%}
	</style>
</head>
<body>
	<h1 style="text-align:center;">Data Peminjam</h1>
	<table border="1" width="100%">
		<tr>
			<th align="center">Id</th>
			<th align="center">Nama</th>
			<th align="center">Tgl Pinjam</th>
			<th align="center">Status Peminjaman</th>
			<th align="center">Nama Pegawai</th>
		</tr>
		<?php
		include "koneksi.php";
		$select=mysql_query("SELECT * FROM peminjam e
			left join pegawai p on p.id_pegawai=e.id_pegawai
			left join inventaris i on i.id_inventaris=e.id_inventaris");
		while($data=mysql_fetch_array($select))
		{
			?>
			<tr>
				<td align="center"><?php echo $data['id_peminjaman']; ?></td>
				<td align="center"><?php echo $data['nama']; ?></td>
				<td align="center"><?php echo $data['tanggal_pinjam']; ?></td>
				<td align="center"><?php echo $data['status_peminjaman']; ?></td>
				<td align="center"><?php echo $data['nama_pegawai']; ?></td>
			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data peminjam.pdf', 'D');
?>