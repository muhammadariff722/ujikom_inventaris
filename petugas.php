	<?php 
	session_start();
	if($_SESSION['id_level'] == 1) {
		true;
	}
	else {
		header("location:home.php");
	}
	if (!isset($_SESSION['username'])) {
		header("location:login.php");
	}
	?>

	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Inventaris Sekolah</title>
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="assets/css/datepicker3.css" rel="stylesheet">
		<link href="assets/css/styles.css" rel="stylesheet">
		<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


		<!--Custom Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<?php include "layouts/navbar-admin.php" ?>

	<?php
	define('nav', TRUE);
	include "layouts/sidebar.php";
	?>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Petugas</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Tabel Petugas
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<a href="input_petugas.php"><button class="btn btn-primary"><i class="fa fa-plus"> Tambah Data Petugas</i></button></a>
						<div class="panel panel-default">
							<div class="panel-body">
								<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>	
										<tr>
											<td> ID </td>
											<td> Username </td>
											<td> Password </td>
											<td> Nama Petugas </td>
											<td> Level </td>
											<td> Pegawai </td>
											<td> Status  </td>
											<td> Aksi </td>
										</tr>
									</thead>
									<tbody>
										<?php
										include "koneksi.php";
										$select=mysql_query("SELECT * FROM petugas p
											left join level l on l.id_level=p.id_level 
											left join pegawai r on r.id_pegawai=p.id_pegawai ");
										while($data=mysql_fetch_array($select))
										{
											?>
											<tr>
												<td><?php echo $data['id_petugas']; ?></td>
												<td><?php echo $data['username']; ?></td>
												<td><?php echo $data['password']; ?></td>
												<td><?php echo $data['nama_petugas']; ?></td>
												<td><?php echo $data['nama_level']; ?></td>
												<td><?php echo $data['nama_pegawai']; ?></td>
												<td><?php echo $data['status']; ?></td>
												<td>
													
													<a class="btn btn-primary" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>">Hapus</a>	
													<a class="btn btn-danger" href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>">Edit</a>							
												</td>
											</tr><?php } ?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<p class="back-link">&copy; 2019</p>
					</div>
				</div><!--/.row-->

			</div>	<!--/.main-->

			<script src="assets/js/jquery-1.11.1.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/jquery.dataTables.min.js"></script>
			<script src="assets/js/dataTables.bootstrap.min.js"></script>
			<script src="assets/js/chart.min.js"></script>
			<script src="assets/js/chart-data.js"></script>
			<script src="assets/js/easypiechart.js"></script>
			<script src="assets/js/easypiechart-data.js"></script>
			<script src="assets/js/bootstrap-datepicker.js"></script>
			<script src="assets/js/custom.js"></script>
			<script>
				$(document).ready(function(){
					$('[data-toggle="tooltip"]').tooltip();
				});
			</script>

			<script type="text/javascript">
				$(document).ready(function() {
					$('#example').DataTable();
				} );
			</script>
			<script>
				window.onload = function () {
					var chart1 = document.getElementById("line-chart").getContext("2d");
					window.myLine = new Chart(chart1).Line(lineChartData, {
						responsive: true,
						scaleLineColor: "rgba(0,0,0,.2)",
						scaleGridLineColor: "rgba(0,0,0,.05)",
						scaleFontColor: "#c5c7cc"
					});
				};
			</script>

		</body>
		</html>
