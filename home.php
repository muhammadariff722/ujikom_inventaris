<?php 
include "session.php";
include "koneksi.php";
?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inventaris Sekolah</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/datepicker3.css" rel="stylesheet">
	<link href="assets/css/styles.css" rel="stylesheet">
	<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<?php include "layouts/navbar-admin.php" ?> 
	<div class="col-md-9 col-md-offset-3">
	<?php 
	if($_SESSION['id_level'] == 1): ?>
	<?php echo "<h2 class='text-center'>Welcome Administrator"."</h2>";?>
	<?php echo "<h3 class='text-left'>Pada Halaman Administrator ini anda bisa mengubah atau mengecek data atau barang apa aja yang berada di gudang dan barang yang sedang di pinjam atau di gunakan oleh siswa siswi"."</h3>";?>
        <?php endif; ?>
	<?php
	 if($_SESSION['id_level'] == 2): ?>
	<?php echo "<h2 class='text-center'>Welcome Oprator"."</h2>";?>
	<?php echo "<h3 class='text-left'>Pada Halaman Oprator ini anda bisa Meminjam Dan mengembalikan apa yang di gunakan atau di pinjam oleh siswa siswi"."</h3>";?>
        <?php endif; ?>
     <?php
	 if($_SESSION['id_level'] == 3): ?>
	<?php echo "<h2 class='text-center'>Welcome Peminjam"."</h2>";?>
	<?php echo "<h3 class='text-left'>Pada Halaman Peminjam ini anda bisa Meminjam Dan tidak bisa untuk mengembalikan apa yang di gunakan atau di pinjam oleh siswa siswi"."</h3>";?>
        <?php endif; ?>
</div>
	<?php
	define('nav', TRUE);
	 include "layouts/sidebar.php" ?>	
<script src="assets/js/jquery3.3.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>