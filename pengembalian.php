<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
}
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en"
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventaris Sekolah</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/datepicker3.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
    <body>
<?php include "layouts/navbar-admin.php" ?>

    <?php
    define('nav', TRUE);
    include "layouts/sidebar.php";
    ?>

                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-8" style="
                            position: relative;
                            left: 17em;">
						<div class="pull-right">
								<a href="inventaris.php"><button type="button" class="btn btn-outline btn-primary fa fa-reply"> Back To Inventarisir </button></a>
						<br/><br/>
							<div class="panel panel-default col-md-12" style="
                                width: 124%;
                                ">
                                <div class="panel-heading">
                                    Pengembalian 
                                </div>
								<div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <form method="post">
											   <select name="id_peminjaman" class="form-control m-bot15">
													<?php
													include "koneksi.php";
															//display values in combobox/dropdown
													$result = mysql_query("SELECT id_peminjaman from peminjam where status_peminjaman='Dipinjam' ");
													while($row = mysql_fetch_assoc($result))
													{
														echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
													} 
													?>
											   </select><br/>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="pilih" value="Tampilkan" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
								<?php if(isset($_POST['pilih'])){?>
								<form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
								<?php
									include "koneksi.php";
									$id_peminjaman=$_POST['id_peminjaman'];
									$select=mysql_query("select * from peminjam 
                                                                        left join detail_pinjam on peminjam.id_peminjaman=detail_pinjam.id_detail_pinjam
                                                                        left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris 
                                                                        left join pegawai on peminjam.id_pegawai=pegawai.id_pegawai
																where id_peminjaman='$id_peminjaman' ");
									while($data=mysql_fetch_array($select))  {
								?>
                        
                                <div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Id Inventaris
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly>
									</div>
								</div><br><br><br>    
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										Tanggal Pinjam
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="tanggal_pinjam">
										<input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
										<input name="tanggal_kembali" type="hidden" required>
									</div>
								</div><br><br>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										ID Pegawai
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" name="" value="<?php echo $data['id_pegawai']?>.<?php echo $data['nama_pegawai'];?>" class="form-control col-md-7 col-xs-12" disabled placeholder="ID Pegawai">
										<input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
									</div>
								</div><br><br>
							  <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
									Jumlah Pinjam
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah">
								</div>
							</div><br><br>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
								 Status Peminjaman
							 </label>
							 <div class="col-md-6 col-sm-6 col-xs-12">
							  <select name="status_peminjaman" class="form-control m-bot15">
								<option><?php echo $data['status_peminjaman']?></option>
								<option>Dikembalikan</option>
							</select>
						</div><br><br><br>
						<div class="form-group">
							&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" /> Selesai</button>
						</div>
					</div>
				<?php } ?>
			</form>
			<?php } ?>
								<div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Tanggal Kembali</th>
                                                    <th>Status Peminjaman</th>
                                                    <th>Nama Pegawai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
								include "koneksi.php";
								$no=1;
								$select=mysql_query("select * from peminjam a 
                                    left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
								while($data=mysql_fetch_array($select))
										{
								?>
                        
                                <tr class="success">
									<td><?php echo $no++; ?></td>
                                    <td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['tanggal_kembali'] ?></td>
									<td><?php echo $data['status_peminjaman'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
                                        </table>
									<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                    </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
		
            <script src="assets/vendor/jquery /jquery.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/jquery.dataTables.min.js"></script>
            <script src="assets/js/dataTables.bootstrap.min.js"></script>
            <script src="assets/js/chart.min.js"></script>
            <script src="assets/js/chart-data.js"></script>
            <script src="assets/js/easypiechart.js"></script>
            <script src="assets/js/easypiechart-data.js"></script>
            <script src="assets/js/bootstrap-datepicker.js"></script>
            <script src="assets/js/custom.js"></script>
            <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('#example').DataTable();
                } );
            </script>

            <script>
                window.onload = function () {
                    var chart1 = document.getElementById("line-chart").getContext("2d");
                    window.myLine = new Chart(chart1).Line(lineChartData, {
                        responsive: true,
                        scaleLineColor: "rgba(0,0,0,.2)",
                        scaleGridLineColor: "rgba(0,0,0,.05)",
                        scaleFontColor: "#c5c7cc"
                    });
                };
            </script>

        </body>
        </html>
