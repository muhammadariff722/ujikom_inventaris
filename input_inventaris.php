	<?php
    session_start();
    include "koneksi.php";
    $alert="";

    if (!isset($_SESSION['username'])) {
        header("location:login.php");
    }
    if (isset($_POST['submit'])) {
        $nama = $_POST['nama'];
        $kondisi = $_POST['kondisi'];
        $keterangan = $_POST['keterangan'];
        $jumlah = $_POST['jumlah'];
        $id_jenis = $_POST['id_jenis'];
        date_default_timezone_set('Asia/Jakarta');
        $tanggal_register = date("Y-m-d G:i:s");
        $id_ruang = $_POST['id_ruang'];
        $kode_inventaris = $_POST['kode_inventaris'];
        $id_petugas = $_POST['id_petugas'];

        $query_add = mysql_query("INSERT INTO inventaris VALUES ('','$nama','$kondisi','$keterangan','$jumlah','$id_jenis','$tanggal_register','$id_ruang','$kode_inventaris','$id_petugas')")or die(mysql_error());
        if ($query_add==true) {
            $alert = header("location: inventaris.php");
        } else {
            $alert = "<div class='alert alert-danger'>
                        Gagal menambahkan data inventaris.
                    </div>";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inventaris Sekolah</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/datepicker3.css" rel="stylesheet">
	<link href="assets/css/styles.css" rel="stylesheet">
	<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
    <script src="ckeditor/ckeditor.js"></script>
</head>
<body>
	<?php include "layouts/navbar-admin.php" ?>

	<?php
	define('nav', TRUE);
		include "layouts/sidebar.php";
	?>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Data Inventaris</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Tambahkan Data Inventaris
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel panel-default">
						<div class="panel-body">
                            <form action="input_inventaris.php" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-9">
                                    <label for="">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <div class="col-sm-9">
                                    <label for="">Kondisi</label>
                                        <input type="text" class="form-control" id="kondisi" name="kondisi" placeholder="Kondisi" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                    <label for="">Keterangan</label>
                                        <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                    <label for="">Jumlah</label>
                                        <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                   <div class="col-sm-9">
                                    <label for="">Jenis</label>
	                                    <select name="id_jenis" class="form-control">
	                                    		<option>--pilih--</option>
	                                    	<?php
	                                    	$select = mysql_query("select * from jenis"); 
	                                    	while ($data = mysql_fetch_array($select)) 
	                                    	{
	                                    		?>
	                                    		<option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
	                                    		<?php } ?>
	                                    	
	                                    </select>
                                </div>
                                </div>  
                                <div class="form-group">
                                   <div class="col-sm-9">
                                    <label for="">Ruang</label>
	                                    <select name="id_ruang" class="form-control">
	                                    		<option>--pilih--</option>
	                                    	<?php
	                                    	$select = mysql_query("select * from ruang"); 
	                                    	while ($data = mysql_fetch_array($select)) 
	                                    	{
	                                    		?>
	                                    		<option value="<?php echo $data['id_ruang'];?>"><?php echo $data['nama_ruang'];?></option>
	                                    		<?php } ?>
	                                    	
	                                    </select>
                                </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                    <label for="">Kode Inventaris</label>
                                        <input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" placeholder="Kode Inventaris" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-sm-9">
                                    <label for="">Petugas</label>
                                    <select name="id_petugas" class="form-control">
	                                   		<option>--pilih--</option>
                                    	<?php
                                    	$select = mysql_query("select * from petugas"); 
                                    	while ($data = mysql_fetch_array($select)) 
                                    	{
                                    		?>
                                    		<option value="<?php echo $data['id_petugas'];?>"><?php echo $data['nama_petugas'];?></option>
                                    		<?php } ?>
                                    	
                                    </select>
                                </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <input type="submit" name="submit" class="btn btn-primary" value="Submit" style="margin-top:10px;">
                                    </div>
                                </div>
                            </form>

						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->

	</div>	<!--/.main-->

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/chart.min.js"></script>
	<script src="assets/js/chart-data.js"></script>
	<script src="assets/js/easypiechart.js"></script>
	<script src="assets/js/easypiechart-data.js"></script>
	<script src="assets/js/bootstrap-datepicker.js"></script>
	<script src="assets/js/custom.js"></script>

	<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>

	<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
		} );
	</script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

</body>
</html>
