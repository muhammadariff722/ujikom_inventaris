<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
		table {border-collapse:collapse; table-layout:fixed;width: 550px:}
		table td {word-wrap:break-word;width: 20%}
</head>
<body>
	<h1 style="text-align:center;">Data Inventaris</h1>
	<table border="1" width="100%">
		<tr>
			<th align="center">No</th>
			<th align="center">Tanggal Pinjam</th>
			<th align="center">Tanggal Kembali</th>
			<th align="center">Status Peminjaman</th>
			<th align="center">Nama Pegawai</th>
		</tr>
		<?php
                       include "koneksi.php";
                       $no=1;
                       $select=mysql_query("select * from peminjam a 
                        left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
                       while($data=mysql_fetch_array($select))
                       {
                        ?>
			<tr>
				<td align="center"><?php echo $no++; ?></td>
				<td align="center"><?php echo $data['tanggal_pinjam']; ?></td>
				<td align="center"><?php echo $data['tanggal_kembali']; ?></td>
				<td align="center"><?php echo $data['status_peminjaman']; ?></td>
				<td align="center"><?php echo $data['nama_pegawai']; ?></td>
			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Pengembalian.pdf', 'D');
?>