<?php 
    if (!defined('nav')) {
        echo "Anda tidak boleh mengakses file ini";
        exit();
    }
 ?>

<div id="sidebar-collapse" class="col-md-3 col-lg-2 sidebar">
    <ul class="nav menu">
    <?php
    if($_SESSION['id_level'] == 1): ?>
        <li><a href="inventaris.php"><em class="fa fa-briefcase">&nbsp;&nbsp;</em>inventaris</a></li>
        <li><a href="peminjam.php"><em class="fa fa-share">&nbsp;&nbsp;</em>Peminjaman</a></li>
        <li><a href="pengembalian.php"><em class="fa fa-navicon">&nbsp;</em>Pengembalian</a></li>
         <li class="parent "><a data-toggle="collapse" href="#sub-item-4">
            <em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
            </a>
            <ul class="children collapse" id="sub-item-4">
                <li>
                    <a class="" href="inventaris_laporan.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Inventaris
                    </a>
                    <a class="" href="peminjam_laporan.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Peminjaman
                    </a>
                     <a class="" href="pengembalian_laporan.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Pengembalian
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent "><a data-toggle="collapse" href="#sub-item-2">
            <em class="fa fa-gear">&nbsp;</em> Master Data <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
            </a>
            <ul class="children collapse" id="sub-item-2">
                <li>

                    <a class="" href="jenis.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Jenis
                    </a>
                    <a class="" href="ruang.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Ruang
                    </a>
                     <a class="" href="petugas.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Petugas
                    </a>
                     <a class="" href="pegawai.php">
                        <span class="fa fa-arrow-right">&nbsp;</span> Pegawai
                    </a>
                </li>
            </ul>
        </li>
        <?php endif; ?>

    <?php if($_SESSION['id_level'] == 2): ?>
        <li><a href="peminjam.php"><em class="fa fa-navicon">&nbsp;</em>Peminjaman</a></li>
        <li><a href="pengembalian.php"><em class="fa fa-navicon">&nbsp;</em>Pengembalian</a></li>
        <?php endif; ?>

    <?php if($_SESSION['id_level'] == 3): ?>
        <li><a href="peminjam.php"><em class="fa fa-navicon">&nbsp;</em>Peminjaman</a></li>
        <?php endif; ?>

</div>
