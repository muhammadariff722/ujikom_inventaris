<?php
	session_start();
    if (!isset($_SESSION['username'])) {
        header("location:login.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inventaris Sekolah</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/datepicker3.css" rel="stylesheet">
	<link href="assets/css/styles.css" rel="stylesheet">
	<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<?php include "layouts/navbar-admin.php" ?>

	<?php
	define('nav', TRUE);
	include "layouts/sidebar.php";
	?>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Peminjaman</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Tabel Peminjaman
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<a href="export_excel_peminjaman.php"><button class="btn btn-primary"><i class="fa fa-share"> Export Excel </i></button></a>
						<a href="export_pdf_peminjam.php"><button class="btn btn-success"><i class="fa fa-share"> Export PDF </i></button></a>
						<div class="panel panel-default">
							<div class="panel-body">
								<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>	
										<tr>
											<td> ID </td>
											<td> Inventaris </td>
											<td> Tanggal Peminjam </td>
											<td> Status Peminjaman </td>
											<td> Pegawai</td>
										</tr>
									</thead>
									<tbody>
										<?php
										include "koneksi.php";
										$select=mysql_query("SELECT * FROM peminjam e
											left join pegawai p on p.id_pegawai=e.id_pegawai
											left join inventaris i on i.id_inventaris=e.id_inventaris");
										while($data=mysql_fetch_array($select))
										{
											?>
											<tr>
												<td><?php echo $data['id_peminjaman']; ?></td>
												<td><?php echo $data['nama']; ?></td>
												<td><?php echo $data['tanggal_pinjam']; ?></td>
												<td><?php echo $data['status_peminjaman']; ?></td>
												<td><?php echo $data['nama_pegawai']; ?></td>
											</tr><?php } ?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<p class="back-link">&copy; 2019</p>
					</div>
				</div><!--/.row-->

			</div>	<!--/.main-->

			<script src="assets/js/jquery-1.11.1.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/jquery.dataTables.min.js"></script>
			<script src="assets/js/dataTables.bootstrap.min.js"></script>
			<script src="assets/js/chart.min.js"></script>
			<script src="assets/js/chart-data.js"></script>
			<script src="assets/js/easypiechart.js"></script>
			<script src="assets/js/easypiechart-data.js"></script>
			<script src="assets/js/bootstrap-datepicker.js"></script>
			<script src="assets/js/custom.js"></script>
			<script>
				$(document).ready(function(){
					$('[data-toggle="tooltip"]').tooltip();
				});
			</script>

			<script type="text/javascript">
				$(document).ready(function() {
					$('#example').DataTable();
				} );
			</script>
			<script>
				window.onload = function () {
					var chart1 = document.getElementById("line-chart").getContext("2d");
					window.myLine = new Chart(chart1).Line(lineChartData, {
						responsive: true,
						scaleLineColor: "rgba(0,0,0,.2)",
						scaleGridLineColor: "rgba(0,0,0,.05)",
						scaleFontColor: "#c5c7cc"
					});
				};
			</script>

		</body>
		</html>
