<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventaris Sekolah</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/datepicker3.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
    <body>

        <?php include "layouts/navbar-admin.php" ?>

    <?php
    define('nav', TRUE);
    include "layouts/sidebar.php";
    ?>

                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-8" style="
                            position: relative;
                            left: 17em;">
                           <div class="panel panel-default col-md-12" style="
                                width: 124%;
                                ">
                                <div class="panel-heading">
                                    Detail Pinjam 
                                </div>
                                <!-- /.panel-heading -->
	                            <div class="panel-body">
								<?php
								include "koneksi.php";
								$id_peminjaman=$_GET['id_peminjaman'];
								$select=mysql_query("select * from peminjam i
                                                                            left join pegawai p on i.id_pegawai=p.id_pegawai
																			left join detail_pinjam d on i.id_peminjaman=d.id_detail_pinjam
																			left join inventaris s on s.id_inventaris=i.id_inventaris
																				where i.id_peminjaman='$id_peminjaman'");
																				while($row=mysql_fetch_array($select)){
								?>
            <table class="table">
                <tr>
                    <td>ID Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $id_peminjaman ?></td>
                </tr>
				<tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $row['nama']; ?></td>
                </tr>
                <tr>
                    <td>Jumlah Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['jumlah_pinjam']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['tanggal_pinjam']; ?></td>
                </tr>
				<tr>
                    <td>Status Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $row['status_peminjaman']; ?></td>
                </tr>
				<tr>
                    <td>Nama Pegawai</td>
                    <td>:</td>
                    <td><?php echo $row['nama_pegawai']; ?></td>
                </tr>
																				<?php } ?>
            </table>
			
								
							<div class="form-group">
								<a href="peminjam.php" ><button type="button" class="btn btn-outline btn-primary fa fa-reply"> Kembali</button></a>
							</div>
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

         <script>
                window.onload = function () {
                    var chart1 = document.getElementById("line-chart").getContext("2d");
                    window.myLine = new Chart(chart1).Line(lineChartData, {
                        responsive: true,
                        scaleLineColor: "rgba(0,0,0,.2)",
                        scaleGridLineColor: "rgba(0,0,0,.05)",
                        scaleFontColor: "#c5c7cc"
                    });
                };
            </script>

        </body>
        </html>

