<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
}
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en"
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventaris Sekolah</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/datepicker3.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php include "layouts/navbar-admin.php" ?>

    <?php
    define('nav', TRUE);
    include "layouts/sidebar.php";
    ?>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Pengembalian</li>
            </ol>
        </div>
        <!--/.row-->
        <div class="panel panel-default col-md-12" style="width: 100%%;">
            <div class="panel-heading">
                Tabel Inventaris
                <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
                <a href="export_excel_pengembalian.php"><button class="btn btn-primary"><i class="fa fa-share"> Export Excel </i></button></a>
                <a href="export_pdf_pengembalian.php"><button class="btn btn-success"><i class="fa fa-share"> Export PDF </i></button></a>
                        <div class="panel panel-default">
                            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Status Peminjaman</th>
                            <th>Nama Pegawai</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       include "koneksi.php";
                       $no=1;
                       $select=mysql_query("select * from peminjam a 
                        left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
                       while($data=mysql_fetch_array($select))
                       {
                        ?>

                        <tr class="success">
                         <td><?php echo $no++; ?></td>
                         <td><?php echo $data['tanggal_pinjam'] ?></td>
                         <td><?php echo $data['tanggal_kembali'] ?></td>
                         <td><?php echo $data['status_peminjaman'] ?></td>
                         <td><?php echo $data['nama_pegawai'] ?></td>
                     </tr>
                     <?php } ?>

                 </tbody>
             </table>
             <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
             <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
   </div>
</div>
</div>
</div>

<script src="assets/vendor/jquery /jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap.min.js"></script>
<script src="assets/js/chart.min.js"></script>
<script src="assets/js/chart-data.js"></script>
<script src="assets/js/easypiechart.js"></script>
<script src="assets/js/easypiechart-data.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/custom.js"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

<script>
    window.onload = function () {
        var chart1 = document.getElementById("line-chart").getContext("2d");
        window.myLine = new Chart(chart1).Line(lineChartData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleFontColor: "#c5c7cc"
        });
    };
</script>

</body>
</html>
