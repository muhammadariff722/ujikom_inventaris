<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
		table {border-collapse:collapse; table-layout:fixed;width: 150px:}
		table td {word-wrap:break-word;width: 8%}
	</style>
</head>
<body>
	<h1 style="text-align:center;">Data Inventaris</h1>
	<table border="1" width="100%">
		<tr>
			<th align="center">Id</th>
			<th align="center">Nama</th>
			<th align="center">Kondisi</th>
			<th align="center">Keterangan</th>
			<th align="center">Jumlah</th>
			<th align="center">Nama Jenis</th>
			<th align="center">Tgl Register</th>
			<th align="center">Nama Ruang</th>
			<th align="center">Kode Inventaris</th>
			<th align="center">Nama Petugas</th>
		</tr>
		<?php
		include "koneksi.php";
		$select=mysql_query("SELECT * FROM inventaris i
										left join jenis j on j.id_jenis=i.id_jenis 
										left join ruang r on r.id_ruang=i.id_ruang 
										left join petugas p on p.id_petugas=i.id_petugas");
		while($data=mysql_fetch_array($select))
		{
			?>
			<tr>
				<td align="center"><?php echo $data['id_inventaris']; ?></td>
				<td align="center"><?php echo $data['nama']; ?></td>
				<td align="center"><?php echo $data['kondisi']; ?></td>
				<td align="center"><?php echo $data['keterangan']; ?></td>
				<td align="center"><?php echo $data['jumlah']; ?></td>
				<td align="center"><?php echo $data['nama_jenis']; ?></td>
				<td align="center"><?php echo $data['tanggal_register']; ?></td>
				<td align="center"><?php echo $data['nama_ruang']; ?></td>
				<td align="center"><?php echo $data['kode_inventaris']; ?></td>
				<td align="center"><?php echo $data['nama_petugas']; ?></td>
			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Inventaris.pdf', 'D');
?>