<?php
    if (!isset($_SESSION)) {
        session_start();
    }

    $alert = "";
    if (isset($_POST['username'])) {
        $user = mysql_real_escape_string($_POST['username']);
        $password = mysql_real_escape_string($_POST['password']);


            include 'koneksi.php';
        $query = mysql_query("SELECT * FROM petugas where username='$user' and password='$password'");

        $r=mysql_fetch_array($query);
        $cek = mysql_num_rows($query);
        if ($cek>0) {
            $_SESSION['username'] = $r['username'];    
            $_SESSION['id_level'] = $r['id_level'];
            header("Location: home.php");
        } else {
            $alert = "
            <div class='alert alert-danger'>
                <strong>Gagal!</strong> username dan password salah.
            </div>
            ";
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login  | </title>

        <?php include "config/content.php" ?>

        <link rel="stylesheet" href="assets/css/login.css">
    </head>
    <body>
        <?php
        include "layouts/navbar.php";
        ?>

        <div class="container">

            <div class="row justify-content-center" style="margin-top:70px;margin-bottom:70px">
                <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                    <form role="form" action="login.php" method="post">
                        <fieldset>
                            <h2>Login</h2>
                            <hr class="colorgraph">
                            <?php echo $alert; ?>
                            <div class="form-group">
                                <input type="text" name="username" id="username"  class="form-control input-lg" maxlength="30" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-lg" maxlength="11" placeholder="Password" required>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <input type="submit" name="submit" class="btn btn-lg btn-info btn-block" value="Masuk">
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

            </div>

        <?php
        include "layouts/footer.php"
        ?>

        <script type="text/javascript" src="assets/js/particle-js"></script>
    </body>
</html>
