<?php
session_start();
if (!isset($_SESSION['username'])) {
	header("location:login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inventaris Sekolah</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/datepicker3.css" rel="stylesheet">
	<link href="assets/css/styles.css" rel="stylesheet">
	<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<?php include "layouts/navbar-admin.php" ?>

	<?php
	define('nav', TRUE);
	include "layouts/sidebar.php";
	?>

	<div class="col-sm-12 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active2">Input</li>	
			</ol>
		</div><!--/.row-->
		<div class="row">

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="x_content">

						<div class="panel-heading">
							Tabel Inventaris
							<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>

							<input type="text" placeholder="Masukan id inventaris" class="form-group col-md-2,5" id="kode" >&nbsp; <a class="btn btn-info fa fa-plus" id="get">Pinjam</a>
							<div class="panel panel-default">
								<div class="panel-body">


									<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>	
											<tr>
												<td> ID Inventaris</td>
												<td> Nama </td>
												<td> Kondisi </td>
												<td> Keterangan </td>
												<td> Jumlah </td>
												<td> Jenis </td>
												<td> Tanggal Register </td>
												<td> Ruang </td>
												<td> Code Inventaris </td>
												<td> Petugas </td>
											</tr>
										</thead>
										<tbody>
											<?php
											include "koneksi.php";
											$select=mysql_query("SELECT * FROM inventaris i
												left join jenis j on j.id_jenis=i.id_jenis 
												left join ruang r on r.id_ruang=i.id_ruang 
												left join petugas p on p.id_petugas=i.id_petugas");
											while($data=mysql_fetch_array($select))
											{
												?>
												<tr>
													<td><?php echo $data['id_inventaris']; ?></td>
													<td><?php echo $data['nama']; ?></td>
													<td><?php echo $data['kondisi']; ?></td>
													<td><?php echo $data['keterangan']; ?></td>
													<td><?php echo $data['jumlah']; ?></td>
													<td><?php echo $data['nama_jenis']; ?></td>
													<td><?php echo $data['tanggal_register']; ?></td>
													<td><?php echo $data['nama_ruang']; ?></td>
													<td><?php echo $data['kode_inventaris']; ?></td>
													<td><?php echo $data['nama_petugas']; ?></td>
												</tr><?php } ?>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<p class="back-link">&copy; 2018</p>
					</div>
				</div><!--/.row-->

			</div>	<!--/.main-->

			<script src="assets/vendor/jquery /jquery.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/jquery.dataTables.min.js"></script>
			<script src="assets/js/dataTables.bootstrap.min.js"></script>
			<script src="assets/js/chart.min.js"></script>
			<script src="assets/js/chart-data.js"></script>
			<script src="assets/js/easypiechart.js"></script>
			<script src="assets/js/easypiechart-data.js"></script>
			<script src="assets/js/bootstrap-datepicker.js"></script>
			<script src="assets/js/custom.js"></script>
			<script>
				$(document).ready(function(){
					$('[data-toggle="tooltip"]').tooltip();
				});
			</script>

			<script type="text/javascript">
				$(document).ready(function() {
					$('#example').DataTable();
				} );
			</script>

			<script type="text/javascript">
				$(document).ready(function(){
					$('#get').click(function(){
						var  kode = document.getElementById("kode").value;

						if (kode) {
							$('.x_content').load('input_peminjaman.php?id='+kode)

						}else {
							alert("FORM HARUS DI ISI")
						}

					})



				})
			</script>

			<script>
				window.onload = function () {
					var chart1 = document.getElementById("line-chart").getContext("2d");
					window.myLine = new Chart(chart1).Line(lineChartData, {
						responsive: true,
						scaleLineColor: "rgba(0,0,0,.2)",
						scaleGridLineColor: "rgba(0,0,0,.05)",
						scaleFontColor: "#c5c7cc"
					});
				};
			</script>

		</body>
		</html>
